<p align="center"> 
  <a href="https://komarev.com/ghpvc/?username=drisskhattabi6">
    <img align="center" src="https://komarev.com/ghpvc/?username=drisskhattabi6&label=Visitors&color=0e75b6&style=flat" alt="Profile visitor" />
</a>
</p> 
  
<!-- Intro  -->
<h3 align="center">
        <samp>&gt; Hey There!, I am
                <b><a target="_blank" href="https://www.linkedin.com/in/idriss-khattabi-b3a266235">Idriss Khattabi</a></b>
        </samp>
</h3>


<p align="center"> 
  <samp>
<!--     <a href="https://www.google.com/search?q=Ayman+Boufarhi">「 Google Me 」</a> -->
<!--     <br> -->
    「 I am an AI & Data Science student and Python Django Developer from <b>Morocco</b> 」
    <!--「 I am an AI & Data Science student and Python Django Developer from <b>Morocco</b> 」-->
    <br>
    <br>
  </samp>
</p>

<p align="center">
 <a href="https://www.linkedin.com/in/idriss-khattabi-b3a266235" target="_blank">
  <img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" alt="idriss-khattabi linkedin"/>
 </a>
 <!-- <a href="https://dev.to/aymanboufarhi" target="_blank">
  <img src="https://img.shields.io/badge/dev.to-0A0A0A?style=for-the-badge&logo=dev.to&logoColor=white" alt="aymanboufarhi" />
 </a> -->
  <!-- 	![Portfolio](https://img.shields.io/badge/Portfolio-%23000000.svg?style=for-the-badge&logo=firefox&logoColor=#FF7139) -->
  <!-- 	![Kaggle](https://img.shields.io/badge/Kaggle-035a7d?style=for-the-badge&logo=kaggle&logoColor=white) -->
  <a href="https://drisskhattabi6.github.io/id-kh/" target="_blank">
  <img src="https://img.shields.io/badge/Portfolio-%23000000.svg?style=for-the-badge&logo=firefox&logoColor=#FF7139" />
 </a>
  <a href="https://www.kaggle.com/idrisskh" target="_blank">
  <img src="https://img.shields.io/badge/Kaggle-035a7d?style=for-the-badge&logo=kaggle&logoColor=white" />
 </a>
 <a href="https://twitter.com/IdrissKhattabi" target="_blank">
  <img src="https://img.shields.io/badge/Twitter-000000?style=for-the-badge&logo=X&logoColor=white" />
 </a>
 <a href="https://www.instagram.com/idriss_khattabi/" target="_blank">
  <img src="https://img.shields.io/badge/Instagram-fe4164?style=for-the-badge&logo=instagram&logoColor=white" alt="aymanboufarhi" />
 </a> 
<!--  <a href="https://leetcode.com/aymanboufarhi1/" target="_blank">
  <img src="https://img.shields.io/badge/LeetCode-FFFFFF?&style=for-the-badge&logo=LeetCode" alt="aymanboufarhi"  />
  </a>  -->
</p>
<br />

<!-- About Section -->
 # About me
 
<p>
<!--  <img align="right" width="350" src="/assets/programmer.gif" alt="Coding gif" /> -->
  
 ✌️ &emsp; Passionate AI and Data Science student<br/><br/>
 ❤️ &emsp; Exploring the possibilities of AI and Data Science for real-world impact<br/><br/>
 📧 &emsp; Reach me anytime: drisskhattabi6@gmail.com<br/><br/>
 💬 &emsp; Ask me about anything and know more about me : 
<a href="https://drisskhattabi6.github.io/id-kh/" target="_blank">
  <img src="https://img.shields.io/badge/Portfolio-%23000000.svg?style=for-the-badge&logo=firefox&logoColor=#FF7139" alt="idriss-khattabi linkedin"/>
 </a>

</p>

<br/>
<br/>
<br/>

## Use To Code :

![Jupyter Notebook](https://img.shields.io/badge/Jupyter%20Notebook-FFA500?style=for-the-badge&labelColor=FFFFFF&logo=jupyter)
![Python](https://img.shields.io/badge/Python-3776ab?style=for-the-badge&labelColor=ffd343&logo=python)
![NumPy](https://img.shields.io/badge/numpy-%23013243.svg?style=for-the-badge&logo=numpy&logoColor=white)
![Matplotlib](https://img.shields.io/badge/Matplotlib-%23ffffff.svg?style=for-the-badge&logo=Matplotlib&logoColor=black)
![Pandas](https://img.shields.io/badge/pandas-%23150458.svg?style=for-the-badge&logo=pandas&logoColor=white)
![scikit-learn](https://img.shields.io/badge/scikit--learn-%23F7931E.svg?style=for-the-badge&logo=scikit-learn&logoColor=white)
![TensorFlow](https://img.shields.io/badge/TensorFlow-%23FF6F00.svg?style=for-the-badge&logo=TensorFlow&logoColor=white)
![Django](https://img.shields.io/badge/django-%23092E20.svg?style=for-the-badge&logo=django&logoColor=white)
![JAVA](https://img.shields.io/badge/Java%20-F05032?style=for-the-badge&logo=JAVA)
![C](https://img.shields.io/badge/-Language%20C%20-61DBFB?style=for-the-badge&labelColor=black&logo=C&logoColor=61DBFB)
![HTML](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white)
![Javascript](https://img.shields.io/badge/Javascript-F0DB4F?style=for-the-badge&labelColor=black&logo=javascript&logoColor=F0DB4F)
![SQLServer](https://img.shields.io/badge/Microsoft%20SQL%20Server-CC2927?style=for-the-badge&logo=microsoft%20sql%20server&logoColor=white)
![MySQL](https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white)
![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white)
![Power Bi](https://img.shields.io/badge/power_bi-F2C811?style=for-the-badge&logo=powerbi&logoColor=black)
![Canva](https://img.shields.io/badge/Canva-%2300C4CC.svg?style=for-the-badge&logo=Canva&logoColor=white)
![Ubuntu](https://img.shields.io/badge/Ubuntu-E95420?style=for-the-badge&logo=ubuntu&logoColor=white)
![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white) 
![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)
![Apache Hadoop](https://img.shields.io/badge/Apache%20Hadoop-66CCFF?style=for-the-badge&logo=apachehadoop&logoColor=black)
![Apache Spark](https://img.shields.io/badge/Apache%20Spark-FDEE21?style=flat-square&logo=apachespark&logoColor=black)
![Visual Studio Code](https://img.shields.io/badge/Visual%20Studio%20Code-0078d7.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white)
<!-- ![MongoDB](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white) -->
<!-- ![Adobe Photoshop](https://img.shields.io/badge/adobe%20photoshop-%2331A8FF.svg?style=for-the-badge&logo=adobe%20photoshop&logoColor=white) -->
<!-- 	![Figma](https://img.shields.io/badge/figma-%23F24E1E.svg?style=for-the-badge&logo=figma&logoColor=white) -->
<!-- 	![ResearchGate](https://img.shields.io/badge/ResearchGate-00CCBB?style=for-the-badge&logo=ResearchGate&logoColor=white) -->
<!-- 	![LeetCode](https://img.shields.io/badge/LeetCode-000000?style=for-the-badge&logo=LeetCode&logoColor=#d16c06) -->
<!-- 	![Strapi](https://img.shields.io/badge/strapi-%232E7EEA.svg?style=for-the-badge&logo=strapi&logoColor=white) -->
<!-- 	![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB) -->
<!-- 	![FastAPI](https://img.shields.io/badge/FastAPI-005571?style=for-the-badge&logo=fastapi) -->
<!-- 	![TypeScript](https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white) -->
<!-- 	![Postman](https://img.shields.io/badge/Postman-FF6C37?style=for-the-badge&logo=postman&logoColor=white) -->
<!-- 	![Arduino](https://img.shields.io/badge/-Arduino-00979D?style=for-the-badge&logo=Arduino&logoColor=white) -->
<!-- 	![Raspberry Pi](https://img.shields.io/badge/-RaspberryPi-C51A4A?style=for-the-badge&logo=Raspberry-Pi) -->
<!-- 	![Portfolio](https://img.shields.io/badge/Portfolio-%23000000.svg?style=for-the-badge&logo=firefox&logoColor=#FF7139) -->
<!--node-red-->
<br/>

## Top Open Source 

[![Chatbot](https://github-readme-stats.vercel.app/api/pin/?username=drisskhattabi6&repo=FSTT_chatbot&border_color=7F3FBF&bg_color=0D1117&title_color=C9D1D9&text_color=8B949E&icon_color=7F3FBF)](https://github.com/drisskhattabi6/FSTT_chatbot)
[![Real Time Twitter Sentiment Analysis](https://github-readme-stats.vercel.app/api/pin/?username=drisskhattabi6&repo=Real-Time-Twitter-Sentiment-Analysis&border_color=7F3FBF&bg_color=0D1117&title_color=C9D1D9&text_color=8B949E&icon_color=7F3FBF)](https://github.com/drisskhattabi6/Real-Time-Twitter-Sentiment-Analysis)

[![Django Blog Project](https://github-readme-stats.vercel.app/api/pin/?username=drisskhattabi6&repo=Django-Blog-Project&border_color=7F3FBF&bg_color=0D1117&title_color=C9D1D9&text_color=8B949E&icon_color=7F3FBF)](https://github.com/drisskhattabi6/Django-Blog-Project)
[![Data Analysis & Machine Learning App](https://github-readme-stats.vercel.app/api/pin/?username=drisskhattabi6&repo=Data-Analysis-And-ML-App&border_color=7F3FBF&bg_color=0D1117&title_color=C9D1D9&text_color=8B949E&icon_color=7F3FBF)](https://github.com/drisskhattabi6/Data-Analysis-And-ML-App)



<p align="left">
  <a href="https://github.com/drisskhattabi6?tab=repositories" target="_blank"><img alt="All Repositories" title="All Repositories" src="https://img.shields.io/badge/-All%20Repos-2962FF?style=for-the-badge&logo=koding&logoColor=white"/></a>
</p>

<br/>
<hr/>
<br/>

<p align="center">
  <a href="https://github.com/drisskhattabi6">
    <img src="https://github-readme-streak-stats.herokuapp.com/?user=drisskhattabi6&theme=radical&border=7F3FBF&background=0D1117" alt="Idriss Khattabi's GitHub streak"/>
  </a>
</p>

<p align="center">
  <a href="https://github.com/drisskhattabi6">
    <img src="https://github-profile-summary-cards.vercel.app/api/cards/profile-details?username=drisskhattabi6&theme=radical" alt="Idriss Khattabi's GitHub Contribution"/>
  </a>
</p>

<a> 
    <a href="https://github.com/drisskhattabi6"><img alt="Idriss Khattabi's Github Stats" src="https://denvercoder1-github-readme-stats.vercel.app/api?username=drisskhattabi6&show_icons=true&count_private=true&theme=react&border_color=7F3FBF&bg_color=0D1117&title_color=F85D7F&icon_color=F8D866" height="192px" width="49.5%"/></a>
  <a href="https://github.com/drisskhattabi6"><img alt="Idriss Khattabi's Top Languages" src="https://denvercoder1-github-readme-stats.vercel.app/api/top-langs/?username=drisskhattabi6&langs_count=8&layout=compact&theme=react&border_color=7F3FBF&bg_color=0D1117&title_color=F85D7F&icon_color=F8D866" height="192px" width="49.5%"/></a>
  <br/>
</a>


![Idriss Khattabi's Graph](https://github-readme-activity-graph.vercel.app/graph?username=drisskhattabi6&bg_color=121212&color=00b3ff&line=db0000&point=ffffff&area=true&hide_border=true)
